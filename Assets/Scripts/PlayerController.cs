using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  [Header("Player Controller")]
  public Transform playerCamera;
  public CharacterController characterController;
  [Space]

  [Header("Player")]
  public float moveSpeed;
  [Range(1f, 9f)]
  public float sensitivity;
  public float jumpForce;
  private const float gravity = -9.81f;

  [Header("Position")]
  private float xRotation;
  private Vector2 playerMouseInput;
  private Vector3 playerMovementInput;
  private Vector3 velocity;

  private void Start()
  {
    Cursor.lockState = CursorLockMode.Locked;
  }

  void Update()
  {
    playerMouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
    playerMovementInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

    MovePlayer();
  }

  private void LateUpdate()
  {
    MovePlayerCamera();
  }

  private void MovePlayer()
  {
    Vector3 moveVector = transform.TransformDirection(playerMovementInput);

    if (characterController.isGrounded)
    {
      velocity.y = -1f;

      if (Input.GetKeyDown(KeyCode.Space))
      {
        velocity.y = jumpForce;
      }
    }
    else
    {
      velocity.y -= gravity * -2f * Time.deltaTime;
    }

    characterController.Move(moveVector * moveSpeed * Time.deltaTime);
    characterController.Move(velocity * Time.deltaTime);
  }

  private void MovePlayerCamera()
  {
    xRotation -= playerMouseInput.y * sensitivity;
    xRotation = Mathf.Clamp(xRotation, -90f, 90f);

    transform.Rotate(Vector3.up * playerMouseInput.x * sensitivity);
    playerCamera.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
  }
}
